use rand::seq::IteratorRandom;
use std::{
    fs::File,
    io::{BufRead, BufReader}
};
use regex::Regex;
use std::io;

use unidecode::unidecode;

fn main() {
    loop {
        start_game();
        println!("Do you want to play again ? (y/n)");
        if !is_play_again() {
            break;
        }
    }
}

fn start_game() {
    let secret_word = find_random_word();
    let mut hidden_secret_word = Regex::new("[a-z]").unwrap().replace_all(&secret_word, "_").to_string();

    println!("The secret word is : {}", hidden_secret_word);

    let mut nb_err = 0;
    let mut is_victory = true;
    let mut tried_letters = Vec::new();

    while hidden_secret_word.contains('_') && nb_err < 6 {
        tried_letters.sort();
        let letter = read_letter(&mut tried_letters);

        if search_and_show_letter(&secret_word, &mut hidden_secret_word, letter) {
            println!("{}", hidden_secret_word);
        } else {
            nb_err += 1;
            draw_pendu(nb_err);
            println!("{}", hidden_secret_word);
            is_victory = false;
        }
    }

    if is_victory {
        println!("WELL DONE !");
    } else {
        println!("GAME OVER ! The word was \"{}\"", secret_word);
    }
}

fn find_random_word() -> String {
    let file = File::open("resources/liste_francais.txt").unwrap_or_else(|e| panic!("File not found: {}", e));
    let buffer_reader = BufReader::new(file);
    let lines = buffer_reader.lines().map(|l| l.expect("Couldn't read line"));
    unidecode(&lines.choose(&mut rand::thread_rng()).expect("File had no lines")).to_lowercase()
}

fn draw_pendu(nb_err: i32) {
    let file = File::open(format!("resources/pendu_{}.txt", &nb_err.to_string())).unwrap_or_else(|e| panic!("File not found: {}", e));
    let buffer_reader = BufReader::new(file);
    let lines = buffer_reader.lines().map(|l| l.expect("Couldn't read line"));
    for line in lines {
        println!("{}", line);
    }
}

fn search_and_show_letter(secret_word: &String, hidden_secret_word: &mut String, letter: char) -> bool {
    let indexes: Vec<_> = secret_word.match_indices(letter).map(|u| u.0).collect();
    *hidden_secret_word = hidden_secret_word.chars().enumerate().map(|(i, c)| if indexes.contains(&i) { letter } else { c }).collect::<String>();
    secret_word.contains(letter)
}

fn read_letter(tried_letters: &mut Vec<char>) -> char {
    println!("Letters already tried : {:?}", tried_letters);

    let mut word = String::new();
    let match_letter = Regex::new("^[a-zA-Z]{1}$").unwrap();
    loop {
        io::stdin().lock().read_line(&mut word).unwrap();

        if match_letter.is_match(&word.trim_end()) {
            let letter = word.to_lowercase().chars().next().unwrap();
            if tried_letters.contains(&letter) {
                println!("You have already tried this letter");
            } else {
                tried_letters.push(letter);
                return letter;
            }
        }

        println!("A single letter is expected");
        word = String::new();
    }
}

fn is_play_again() -> bool {
    let mut word = String::new();
    io::stdin().lock().read_line(&mut word).unwrap();

    if !word.is_empty() {
        let letter: char = word.to_lowercase().chars().next().unwrap();
        return letter == 'y';
    }

    false
}
